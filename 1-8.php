<?php
$error = "";
$name = "";
$age = "";
$email = "";
$address = "";

function clean_text($string){
	
	$string = trim($string);
	$string = stripslashes($string);
	$string = htmlspecialchars($string);
	return $string;
}

if(isset($_POST["submit"])){

	$name = $_POST["name"];
	if(!preg_match("/^[a-z A-Z]*$/", $name)){
		$error .= '<p><label class="text-danger">Letters and Whitespaces only</label></p>';
	}

	$email = clean_text($_POST["email"]);
	if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$error .= '<p><label class="text-danger">Email Invalid Format</label></p>';
	}

	$age = clean_text($_POST["age"]);
	$address = clean_text($_POST["address"]);
	
	}

	if($error == '')
	{
		$file_open = fopen("contact_data.csv", "a");
		$no_rows = count(file("contact_data.csv"));
		if($no_rows > 1)
		{
			$no_rows = ($no_rows - 1) + 1;
		}
		$form_data = array(
			// 'sr_no' => $no_rows,
			'name'  => $name,
			'email' => $email,
			'age'   => $age,
			'address' => $address
			);
			fputcsv($file_open, $form_data);
			$error .= '<p><label class="text-success">Thank you</label></p>';
			$name = "";
			$age = "";
			$email = "";
			$address = "";
	}

?>

<html>
<body>
<h1 align="middle"> User Information</h1>
<br>
<form name = "myForm" method = "POST" align="middle">
<?php echo $error; ?>

<br>
Name: <input type = "text" name="name" value = "<?php echo $name; ?>"required>
<br><br>
Age: <input type = "number" name="age" required value= "<?php echo $age; ?>
">
<br><br>
Email: <input type = "text" name="email" required value="<?php echo $email; ?>" >
<br><br>
Address: <input type = "text" name="address" required value="<?php echo $address; ?>" >
<br><br>
<input type = "submit" name="submit" value="Submit">

</form>
</body>
</html>