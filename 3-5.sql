-- 3-5.1)
SELECT * FROM `employees` WHERE `last_name` LIKE 'K%'

-- 3-5.2)
SELECT * FROM `employees` WHERE `last_name` LIKE '%i'

-- 3-5.3)
SELECT `first_name`, `middle_name`, `last_name`, `hire_date` FROM employees
WHERE `hire_date`
           BETWEEN '2015-01-01' 
                 AND '2015-03-31' ORDER BY hire_date ASC 

-- 3-5.4)
select e.last_name, k.`last_name` from employee e INNER JOIN employee k where e.boss_id= k.id

-- 3-5.5)
SELECT `last_name` FROM employees WHERE `department_id` = 3 ORDER BY `last_name` DESC

-- 3-5.6)
SELECT COUNT(`middle_name`)
FROM employees
WHERE `middle_name` IS NOT NULL;

-- 3-5.7)
SELECT departments.name, COUNT(employees.department_id) AS dep_count FROM departments LEFT JOIN employees ON departments.id = employees.department_id WHERE department_id <> 0 GROUP BY departments.id

-- 3-5.8)
 select `first_name`,`middle_name`,`last_name` from employees where `hire_date` IN(select max(`hire_date`) from
   employees)

-- 3-5.9)
Select `name`
 From departments
  Left Outer Join employees ON employees.department_id = departments.id
 Where employees.department_id IS NULL

 -- 3-5.10)
SELECT `first_name`,`middle_name`,`last_name`, COUNT(*) FROM employees LEFT OUTER JOIN employee_positions on employee_positions.employee_id = employees.id GROUP BY employees.id HAVING COUNT(*)>1