-- 3-2.sql
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` int(7) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 3-3.sql
INSERT INTO `user` (`user_id`, `name`, `phone`, `email`, `address`) VALUES
(1, 'Maria Alonzo', 1234567, 'mariaalonzo@gmail.com', 'Blk 1 Lot 3 Lon Subd. Malabon City'),
(2, 'Ryan Santos', 3472819, 'ryansantos@yahoo.com', '10th floor Estey Residences Makati City'),
(3, 'Patricia Arellano', 8932019, 'patriciaarellano@gmail.com', 'Blk 4 Lot 22 Narra Subd. Las Pinas City'),
(4, 'James Roel', 8932873, 'jamesroel@yahoo.com', '9th floor City Helm Residences Manila City');

UPDATE user SET name = 'Maria Liana Alonso' WHERE name = 'Maria Alonzo'
UPDATE user SET `email` ='user@gmail.com'  WHERE `name` = 'Ryan Santos' 

delete from user where `email` = 'patriciaarellano@gmail.com'
delete from user where `name` = 'James Roel'

-- 3-4.sql
-- WHERE
SELECT * FROM `user` WHERE `name` = 'Maria Liana Alonso'

-- HAVING
SELECT COUNT(`user_id`), `phone`
FROM user
HAVING COUNT(`user_id`) > 3;

-- GROUP BY
SELECT COUNT(`name`), `address`
FROM user
GROUP BY `address`;

-- ORDER BY
SELECT * FROM USER
ORDER BY `address` DESC;

-- INNER JOIN
SELECT user.`user_id`, user.name, customer_table.`Name`
FROM user
INNER JOIN customer_table ON user.`user_id` = customer_table.`user_id`;

-- OUTER JOINS
SELECT user.address_id, user.name, customer_table.Name ,customer_table.Orders
FROM user
LEFT JOIN customer_table ON user.address_id = customer_table.address_id
ORDER BY user.name;

SELECT user.user_id, user.name, customer_table.Name ,customer_table.Orders FROM user RIGHT JOIN customer_table ONuser.user_id = customer_table.user_id ORDER BY user.name

