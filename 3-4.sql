-- (WHERE)
SELECT * FROM `user` WHERE `name` = 'Maria Liana Alonso'

-- (HAVING)
SELECT COUNT(`user_id`), `phone`
FROM user
HAVING COUNT(`user_id`) > 3;

-- (GROUP BY)
SELECT COUNT(`name`), `address`
FROM user
GROUP BY `address`;

-- (ORDER BY)
SELECT * FROM USER
ORDER BY `address` DESC;

-- (INNER JOINS)
SELECT user.`user_id`, user.name, customer_table.`Name`
FROM user
INNER JOIN customer_table ON user.`user_id` = customer_table.`user_id`;

-- (OUTER JOINS)
SELECT user.address_id, user.name, customer_table.Name ,customer_table.Orders
FROM user
LEFT JOIN customer_table ON user.address_id = customer_table.address_id
ORDER BY user.name;


SELECT user.user_id, user.name, customer_table.Name ,customer_table.Orders FROM user RIGHT JOIN customer_table ONuser.user_id = customer_table.user_id ORDER BY user.name

