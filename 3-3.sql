-- INSERT
INSERT INTO `user` (`user_id`, `name`, `phone`, `email`, `address`) VALUES
(1, 'Maria Alonzo', 1234567, 'mariaalonzo@gmail.com', 'Blk 1 Lot 3 Lon Subd. Malabon City'),
(2, 'Ryan Santos', 3472819, 'ryansantos@yahoo.com', '10th floor Estey Residences Makati City'),
(3, 'Patricia Arellano', 8932019, 'patriciaarellano@gmail.com', 'Blk 4 Lot 22 Narra Subd. Las Pinas City'),
(4, 'James Roel', 8932873, 'jamesroel@yahoo.com', '9th floor City Helm Residences Manila City');

-- UPDATE
UPDATE user SET name = 'Maria Liana Alonso' WHERE name = 'Maria Alonzo'
UPDATE user SET `email` ='user@gmail.com'  WHERE `name` = 'Ryan Santos' 

-- DELETE
delete from user where `email` = 'patriciaarellano@gmail.com'
delete from user where `name` = 'James Roel'
