<!DOCTYPE html>
<body>
<head>
<style>
form {
	width:700px;
	padding: 10px;
	margin: 60px auto;
	background: lightblue;
	 border-radius: 20px;s
    -webkit-border-radius:10px;
    -moz-border-radius: 10px;
    box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.13);
    -moz-box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.13);
    -webkit-box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.13);
}
h3 {
	display: block;
	margin-top: 2px;
	font-size: 45px ;
}
input {
	height: 20px;
}

table {
	margin: 40px auto;
}
</style>
</head>
<form action="insert.php" method="POST">
<h3 align="middle">User Information</h3>
<table>
<tr>
<td>Name: </td>
<td><input type="name" name="username" pattern="([A-Za-z]).{6,}" title="Invalid name format" required></td>
</tr>
<tr>
<td>Password: </td>
<td><input type="password" name="password" pattern=".{6,}" title="Six or more characters" required></td>
</tr>
<tr>
<td>Gender: </td>
<td>
<input type="radio" name="gender" value="m" required>Male
<input type="radio" name="gender" value="f" required>Female
</td>
</tr>
<tr>
<td>Email: </td>
<td><input type="email" name="email" title="Invalid email format" required></td>
</tr>
<tr>
<td>Phone No: </td>
<td><input type="text" name="phone" pattern="[0-9]{7}" title="Invalid Phone number" required></td>
</tr>
<tr>
<td><br><input type="submit" value="Submit"></td>
</tr>
</table>
</form>
</body>
</html>